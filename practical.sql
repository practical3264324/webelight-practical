-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2023 at 08:15 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `practical`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(7, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(8, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(9, '2016_06_01_000004_create_oauth_clients_table', 2),
(10, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(11, '2023_06_17_160200_add_role_to_users_table', 3),
(12, '2023_06_18_112646_create_products_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `scopes` text DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('63d0632aaacc9ab4b49ca0c97e948082460e0a080f9fba3b95c2668c1efa9e50cc87cf5c0281259f', 1, 1, 'API Token Demo', '[]', 0, '2023-06-17 10:40:10', '2023-06-17 10:40:11', '2024-06-17 16:10:10'),
('771e37f7ed1d6d977b3fe96d5ea2224b677268a2db1e7d17f0aadcd24613f71c79848791ee2b1f79', 14, 1, 'API Token Demo', '[]', 0, '2023-06-17 10:40:25', '2023-06-17 10:40:25', '2024-06-17 16:10:25'),
('d65c51f2fb60310d5ddbbbae0ccdd5f9020c7de4a66ffcdef33914c410b7dff74d18a657abf293ed', 1, 1, 'API Token Demo', '[]', 0, '2023-06-17 09:09:07', '2023-06-17 09:09:07', '2024-06-17 14:39:07');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `secret` varchar(100) DEFAULT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `redirect` text NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'jXnFuJo78ZDxCP7KGKPUD3WHJOezEm6A2pDStJYb', NULL, 'http://localhost', 1, 0, 0, '2023-06-17 08:38:04', '2023-06-17 08:38:04'),
(2, NULL, 'Laravel Password Grant Client', 'kpXTmIF4habYViuHYJKB8sFTAVvNMjwojHfbGow6', 'users', 'http://localhost', 0, 1, 0, '2023-06-17 08:38:04', '2023-06-17 08:38:04');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2023-06-17 08:38:04', '2023-06-17 08:38:04');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) NOT NULL,
  `access_token_id` varchar(100) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `detail`, `created_by`, `created_at`, `updated_at`) VALUES
(2, 'product2', 'dsgdfgdf', 13, '2023-06-18 07:40:52', '2023-06-18 07:40:52'),
(3, 'Product1', 'fdgdfgdf', 13, '2023-06-18 07:58:42', '2023-06-18 07:58:42'),
(4, 'Product3', 'fdgfdgdfg', 14, '2023-06-18 07:59:28', '2023-06-18 07:59:28'),
(5, 'Product4', 'gdfjhcvbdsfv', 14, '2023-06-18 07:59:49', '2023-06-18 07:59:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` varchar(255) NOT NULL DEFAULT 'customer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES
(1, 'a1', 'a1@mailinator.com', NULL, '$2y$10$7KFya1AGIo/ZHkTPqmhvQOvlnUb1ftLE3TQdAJ9UQ92O1XFgRpJcu', NULL, '2023-06-17 09:08:53', '2023-06-17 09:08:53', 'customer'),
(2, 'a2', 'a2@mailinator.com', NULL, '$2y$10$cXeAWvhVoGG441xFHp7oWuG.poT7Z4mbCHk2Srtf0uqTT5wDRWp4m', NULL, '2023-06-17 09:25:38', '2023-06-17 09:25:38', 'customer'),
(3, 'a3', 'a3@mailinator.com', NULL, '$2y$10$k.Wt0wUr1A3TT63V5yR6X.WVA7h/69JoFGNsu47Qm/Z3XvumQYPcu', NULL, '2023-06-17 09:27:49', '2023-06-17 09:27:49', 'customer'),
(4, 'a4', 'a4@mailinator.com', NULL, '$2y$10$te6QOgWcmjUYYtk5SFwaHeOvi7s/3meLcu2YB956mYrm7uuvbMxoS', NULL, '2023-06-17 09:33:27', '2023-06-17 09:33:27', 'customer'),
(5, 'a5', 'a5@mailinator.com', NULL, '$2y$10$T8jScBL3E/RT772IxhR8nu2R8BCiaDvrYj3NGlLw3Xy7pOL7DLMke', NULL, '2023-06-17 09:36:34', '2023-06-17 09:36:34', 'customer'),
(6, 'a6', 'a6@mailinator.com', NULL, '$2y$10$nGGRmWsHecOWHbX4HQufWe98DGZxcLlqwZZr6dXcF144lrBLOJfa6', NULL, '2023-06-17 09:42:51', '2023-06-17 09:42:51', 'customer'),
(7, 'a7', 'a7@mailinator.com', NULL, '$2y$10$dsE6coUk.NNOxQf77Af8CuYI4CB6cC7C5Mu3xbXPptmQTzvHcpFrq', NULL, '2023-06-17 09:44:43', '2023-06-17 09:44:43', 'customer'),
(8, 'a8', 'a8@mailinator.com', NULL, '$2y$10$1evR37ufDYmTam8xBs3yEOLCqop9PpwrtDchC96cA8qYWZd140LW.', NULL, '2023-06-17 09:52:02', '2023-06-17 09:52:02', 'customer'),
(9, 'a9', 'a9@mailinator.com', NULL, '$2y$10$Rt9ScqFYpJq6oz6AELye7.d.et21VQc72pOpov9rYaexC1kQg8fzK', NULL, '2023-06-17 09:52:35', '2023-06-17 09:52:35', 'customer'),
(10, 'a10', 'a10@mailinator.com', NULL, '$2y$10$CTVwWNbZybLlY4eU694Yx.TWYw9PfweicYPyDXfWhDVQIzmR2PxHi', NULL, '2023-06-17 09:56:49', '2023-06-18 09:10:15', 'customer'),
(11, 'a11', 'a11@mailinator.com', NULL, '$2y$10$nGq80gJnD9Lrm4Xl8zf60O1/1YhGMhX4sursKSZc23z0m90RZJeXa', NULL, '2023-06-17 10:01:36', '2023-06-18 08:52:49', 'customer'),
(12, 'a12', 'a12@mailinator.com', NULL, '$2y$10$KpwDGHNiwVwZMLT.KkcRz.SfMCJOLsv6CXJVc1V54exKX9Tk25gwC', NULL, '2023-06-17 10:10:50', '2023-06-17 10:10:50', 'customer'),
(13, 'a13', 'a13@mailinator.com', NULL, '$2y$10$d5eS1/3dgnZhqfmWPdkP0.MwssjTsuZ.KU9oex2roZTPNoc8m2rge', NULL, '2023-06-17 10:16:29', '2023-06-17 10:16:29', 'customer'),
(14, 'a14', 'a14@mailinator.com', NULL, '$2y$10$R1WvM8NadmFUwNlhBTjk9.9Gmp86LSU5h/u9esDLiZItjytHOczKy', NULL, '2023-06-17 10:38:08', '2023-06-18 09:10:07', 'customer'),
(16, 'admin', 'admin@admin.com', NULL, '$2y$10$VjYiO4S3fLaG9RwBQ2gCMuCJ9uEzuBE9T.XGX9/fw1kmU.TQM1bWO', NULL, '2023-06-18 10:37:20', '2023-06-18 10:37:20', 'admin'),
(18, 'user1', 'user1@mailinator.com', NULL, '$2y$10$wNMSUgdBdFs8/77BJIKO4.QQedr8pX9zOjvO76WVDRuugK4SEgqgG', NULL, '2023-06-18 12:24:29', '2023-06-18 12:24:29', 'customer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
