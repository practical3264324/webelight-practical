@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header with-border">
                        <h3 class="card-title">Edit Profile </h3>
                    </div></br>
                    <div class="col-md-12 pad margin no-print">
                        <div style="margin-bottom: 0!important;" class="callout callout-info">
                            <h4><i class="fa fa-info"></i> Note:</h4>
                            Leave <strong>Password</strong> empty if you are not
                            going to
                            change the password.
                        </div>
                    </div>

                    <form id="profile_frm" name="profile_frm" method="POST">
                        @csrf
                      <div class="card-body">
                          <input type="hidden" name="id" id="id" value="{{$user->id}}">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>name <span class="red">*</span></label>
                                <input type="text" class="form-control" placeholder="Please enter name" id="name" name="name" value="{{$user->name}}">
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Email <span class="red">*</span></label>
                                <input type="text" class="form-control" placeholder="Please enter email" id="email" name="email" value="{{$user->email}}">
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="Please enter password" id="password" name="password">
                              </div>
                            </div>

                          </div>
                      </div>
                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right" id="btn">Update <span style="display: none" id="loader"><i class="fa fa-spinner fa-spin"></i></span></button>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
        <script>
            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                //validate and form submit on add and update user
                $("#profile_frm").validate({
                    rules: {
                        name: "required",
                        role: "required",
                        email: "required",
                        // password: "required"
                    },
                    messages: {
                        name: "Required name",
                        role: "Required role",
                        email: "Required email",
                        // password: "Required password"
                    },
                    submitHandler: function(form,e) {
                        e.preventDefault();
                        $.ajax({
                            url: '{{ route("users.store") }}',
                            type: 'POST',
                            data: new FormData(form),
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            success: function (response) {
                                if(response.status==true){
                                    toastr.success(response.message);
                                    location.reload();
                                }
                                else{
                                    toastr.error(response.message);
                                }
                            },
                            error:function(){
                                toastr.error('Please Reload Page');
                            }
                        });
                        return false;
                    }
                });

            });
        </script>
    @endpush
