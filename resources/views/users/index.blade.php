@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <button name="add_user" id="add_user" class="btn btn-primary float-right">Add User</button>

                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th width="100px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                        <div class="modal fade" id="AddUserModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Add User</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="POST" name="addUser" id="addUser">
                                        <div class="modal-body">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Name</label>
                                                        <input type="text" class="form-control" name="name"
                                                            placeholder="Enter name" id="name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Role</label>
                                                        {{-- <input type="text" class="form-control" name="role"
                                                            placeholder="Enter role" id="role"> --}}
                                                            <select name="role" id="role" class="form-control">
                                                                <option value="customer">Customer</option>
                                                                <option value="admin">Admin</option>
                                                            </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Email address</label>
                                                        <input type="email" class="form-control" name="email"
                                                            placeholder="Enter email" id="email">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Password</label>
                                                        <input type="password" class="form-control" name="password"
                                                            placeholder="Password" id="password">
                                                    </div>
                                                </div>
                                                <input type="hidden" name="id" id="id">
                                            </div>

                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" id="addUserBtn">Save</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('js')
        <script>
            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                //datatable
                var table = $('.data-table').DataTable({
                    processing: true,
                    responsive:true,
                    serverSide: true,
                    order : [],
                    ajax: "{{ route('users.index') }}",
                    order:[],
                    columns: [
                        {data: 'DT_RowIndex',name: 'DT_RowIndex',orderable: false,searchable: false},
                        {data: 'name'},
                        {data: 'email'},
                        {data: 'role'},
                        {data: 'action',orderable: false,searchable: false},
                    ]
                });

                $('#add_user').click(function(){
                    $('#id').val('');
                    $('#addUser')[0].reset();
                    $('#AddUserModal').modal('show');
                });

                //validate and form submit on add and update user
                $("#addUser").validate({
                    rules: {
                        name: "required",
                        role: "required",
                        email: "required",
                        // password: "required"
                    },
                    messages: {
                        name: "Required name",
                        role: "Required role",
                        email: "Required email",
                        // password: "Required password"
                    },
                    submitHandler: function(form,e) {
                        e.preventDefault();
                        $.ajax({
                            url: '{{ route("users.store") }}',
                            type: 'POST',
                            data: new FormData(form),
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            success: function (response) {
                                if(response.status==true){
                                    $('#AddUserModal').modal('hide');
                                    toastr.success(response.message);
                                    table.ajax.reload();
                                }
                                else{
                                    toastr.error(response.message);
                                }
                            },
                            error:function(){
                                $('#AddUserModal').modal('hide');
                                toastr.error('Please Reload Page');
                            }
                        });
                        return false;
                    }
                });

                //edit ajax
                $(document.body).on('click', '.edit', function () {

                    var $alertas = $('#addUser');
                    $alertas.trigger('reset');
                    $alertas.find('.error').removeClass('error');
                    $alertas.find('.pac-target-input-error').removeClass('pac-target-input-error');

                    let id = $(this).attr('data-id');
                    $.ajax({
                        type: "GET",
                        url: "{{url('users')}}" + '/' + id,
                        dataType: "json",
                        success: function (response) {
                            if(response.status){
                                $('#name').val(response.data.name);
                                $("#role").val(response.data.role).change();
                                $('#id').val(response.data.id);
                                $('#email').val(response.data.email);
                                $('#AddUserModal').modal('show');
                                // toastr.success(response.message);
                                table.ajax.reload();

                            } else {
                                toastr.error(response.message);
                            }
                        },
                        error: function () {
                            $('#AddUserModal').modal('hide');
                            toastr.error('Please reload page');
                        }
                    });
                });

                //delete ajax
                $(document.body).on('click', '.delete', function () {
                    let id = $(this).attr('data-id');

                        $.ajax({
                            type: "DELETE",
                            url: "{{url('users')}}" + '/' + id,
                            dataType: "json",
                            success: function (response) {
                                toastr.success(response.message);
                                table.ajax.reload();
                            },
                            error: function () {
                                toastr.error('Please reload page');
                            }
                        });
                });

                //edit ajax
                $(document.body).on('click', '.send_email', function () {

                    let id = $(this).attr('data-id');
                    $.ajax({
                        type: "GET",
                        url: "{{url('send_email')}}" + '/' + id,
                        dataType: "json",
                        success: function (response) {
                            if(response.status){
                                toastr.success(response.message);

                            } else {
                                toastr.error(response.message);
                            }
                        },
                        error: function () {
                            toastr.error('Please reload page');
                        }
                    });
                });
            });
        </script>
    @endpush
@endsection
