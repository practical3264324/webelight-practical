@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    {{-- @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Welcome to dashboard') }} --}}
                    <section class="content">
                        <div class="container-fluid">
                          <!-- Small boxes (Stat box) -->
                          <div class="row">
                            {{-- users --}}
                            @if(Auth::user()->isAdmin())
                            <div class="col-lg-3 col-6">
                              <!-- small box -->
                              <div class="small-box bg-info">
                                <div class="inner">
                                  <h3>{{$user_count}}</h3>

                                  <p>Users</p>
                                </div>
                                <div class="icon">
                                  <i class="ion ion-bag"></i>
                                </div>
                                <a href="{{route('users.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                              </div>
                            </div>
                            @endif
                            <!-- ./col -->

                            {{-- products --}}
                            <div class="col-lg-3 col-6">
                              <!-- small box -->
                              <div class="small-box bg-success">
                                <div class="inner">
                                  <h3>{{$product_count}}</h3>

                                  <p>Products</p>
                                </div>
                                <div class="icon">
                                  <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{route('products.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                              </div>
                            </div>

                          </div>
                          <!-- /.row -->
                        </div><!-- /.container-fluid -->
                      </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
