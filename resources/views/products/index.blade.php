@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <button name="add_product" id="add_product" class="btn btn-primary float-right">Add Product</button>

                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Detail</th>
                                    <th>Created By</th>
                                    <th width="100px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                        <div class="modal fade" id="AddProductModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Add Product</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="POST" enctype='multipart/form-data' name="addProduct" id="addProduct">
                                        <div class="modal-body">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Name</label>
                                                        <input type="text" class="form-control" name="name"
                                                            placeholder="Enter name" id="name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Detail</label>
                                                        <input type="text" class="form-control" name="detail"
                                                            placeholder="Enter detail" id="detail">
                                                    </div>
                                                </div>
                                                <input type="hidden" name="id" id="id">
                                            </div>

                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" id="addProductBtn">Save</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('js')
        <script>
            $(document).ready(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                //datatable
                var table = $('.data-table').DataTable({
                    processing: true,
                    responsive:true,
                    serverSide: true,
                    order : [],
                    ajax: "{{ route('products.index') }}",
                    order:[],
                    columns: [
                        {data: 'DT_RowIndex',name: 'DT_RowIndex',orderable: false,searchable: false},
                        {data: 'name'},
                        {data: 'detail'},
                        {data: 'created_by'},
                        {data: 'action',orderable: false,searchable: false},
                    ]
                });

                $('#add_product').click(function(){
                    $('#id').val('');
                    $('#addProduct')[0].reset();
                    $('#AddProductModal').modal('show');
                });

                //validate and form submit on add and update product
                $("#addProduct").validate({
                    rules: {
                        name: "required",
                        detail: "required",
                        // password: "required",
                        // image: "required"
                    },
                    messages: {
                        name: "Required name",
                        detail: "Required detail",
                        // password: "Required password",
                        // image: "Required image"
                    },
                    submitHandler: function(form,e) {
                        e.preventDefault();
                        $.ajax({
                            url: '{{ route("products.store") }}',
                            type: 'POST',
                            data: new FormData(form),
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            success: function (response) {
                                if(response.status==true){
                                    $('#AddProductModal').modal('hide');
                                    toastr.success(response.message);
                                    table.ajax.reload();
                                }
                                else{
                                    toastr.error(response.message);
                                }
                            },
                            error:function(){
                                $('#AddProductModal').modal('hide');
                                toastr.error('Please Reload Page');
                            }
                        });
                        return false;
                    }
                });

                //edit ajax
                $(document.body).on('click', '.edit', function () {

                    var $alertas = $('#addProduct');
                    $alertas.trigger('reset');
                    $alertas.find('.error').removeClass('error');
                    $alertas.find('.pac-target-input-error').removeClass('pac-target-input-error');

                    let id = $(this).attr('data-id');
                    $.ajax({
                        type: "GET",
                        url: "{{url('products')}}" + '/' + id,
                        dataType: "json",
                        success: function (response) {
                            if(response.status){
                                $('#name').val(response.data.name);
                                $('#id').val(response.data.id);
                                $('#detail').val(response.data.detail);
                                $('#AddProductModal').modal('show');
                                // toastr.success(response.message);
                                table.ajax.reload();

                            } else {
                                toastr.error(response.message);
                            }
                        },
                        error: function () {
                            $('#AddProductModal').modal('hide');
                            toastr.error('Please reload page');
                        }
                    });
                });

                //delete ajax
                $(document.body).on('click', '.delete', function () {
                    let id = $(this).attr('data-id');

                        $.ajax({
                            type: "DELETE",
                            url: "{{url('products')}}" + '/' + id,
                            dataType: "json",
                            success: function (response) {
                                toastr.success(response.message);
                                table.ajax.reload();
                            },
                            error: function () {
                                toastr.error('Please reload page');
                            }
                        });
                });
            });
        </script>
    @endpush
@endsection
