<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_count=User::count();
        $product_count=Product::when(Auth::user()->role=="customer",function($q){
            return $q->where('created_by',Auth::user()->id);
        })->count();
        return view('home')->with(['title'=>'Dashboard','user_count'=>$user_count,'product_count'=>$product_count]);
    }
}
