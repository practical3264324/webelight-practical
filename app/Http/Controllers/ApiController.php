<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class ApiController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'role' => 'required|string|in:admin,customer'
        ]);
        $data['password'] = bcrypt($request->password);
        $user = User::create($data);
        $user->sendEmailVerificationNotification();
        return response()->json(['status'=>true,'message'=>"Verification email successfully sent on your email id",'data'=>$user]);
    }

    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($data)) {
            return response(['error_message' => 'Incorrect Details.
            Please try again']);
        }

        $token = auth()->user()->createToken('API Token Demo')->accessToken;
        return response()->json(['status'=>true,'message'=>"Successfully Logged In",'data' => auth()->user(), 'token' => $token]);

    }
}
