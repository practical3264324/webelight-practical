<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use App\Models\User;
use Illuminate\Http\Request;
use Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Throwable;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<div class="actions-a">
                    <a class="btn btn-primary btn-sm mr-1 edit"  data-id=' . $row->id . ' href="javascript:void(0)" title="Edit"><i class="fas fa-edit white cicon"></i> </a>
                    <a data-id=' . $row->id . ' class="btn btn-danger btn-sm mr-1 delete" href="javascript:void(0)" title="Delete"><i class="fas fa-trash-alt white cicon"></i></a>
                    <a data-id=' . $row->id . ' class="btn btn-success btn-sm mr-1 send_email" href="javascript:void(0)" title="Send Email"><i class="fas fa-solid fa-envelope white cicon"></i></a>
                    </div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('users.index')->with(['title'=>'Users']);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $post=$request->all();

            //if else condition here is based on password given or not given while edit
            if($request->id){
                //for password
                if(isset($request->password) && !empty($request->password)){
                    $post['password']=bcrypt($request->password);
                }else{
                    unset($post['password']);
                }
            }else{
                $post['password']=bcrypt($request->password);
            }
            unset($post['id']);
            $request->id ?? 0;
            $user=User::updateOrCreate(['id'=>$request->id],$post);
            $message=$request->id=='' ? 'User successfully Inserted' : 'User successfully Updated';
            return response()->json(['status' => true,'message' => $message]);
        }
        catch(Throwable $th){
            return response()->json(['status'=> false,'message' => $th->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try{
            $data=User::whereId($id)->firstorFail();
            return response()->json(['status'=>true,'message'=>"Successfully Fetched Data",'data'=>$data]);
        }
        catch(Throwable $th){
            return response()->json(['status'=>false,'message' => $th->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        User::find($id)->delete();
        return response()->json(['status' => true, 'message' => 'Successfully Deleted User']);
    }

    public function send_email(string $id)
    {
        try{
            $user=User::find($id);
            $password=Str::random(8);
            $user->password = Hash::make($password);
            $user->save();
            $data = array(
                'name'      =>  $user->name,
                'password'   => $password
            );

         Mail::to($user->email)->send(new SendMail($data));
         return response()->json(['status'=>true,'message'=>"Successfully Sent Email"]);
        }
        catch(Throwable $th){
            return response()->json(['status'=>false,'message' => $th->getMessage()]);
        }
    }

    public function profile()
    {
        $user = Auth::user();
        return view('profile.profile',)->with('title', 'Profile')->with('user', $user);
    }
}
