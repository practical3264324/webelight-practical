<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use Throwable;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::with('user')->when(Auth::user()->role=="customer",function($q){
                return $q->where('created_by',Auth::user()->id);
            })->latest();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('created_by', function (Product $product) {
                    return $product->user->name;
                })
                ->addColumn('action', function ($row) {
                    $btn = '<div class="actions-a">
                    <a class="btn btn-primary btn-sm mr-1 edit"  data-id=' . $row->id . ' href="javascript:void(0)" title="Edit"><i class="fas fa-edit white cicon"></i> </a>
                    <a data-id=' . $row->id . ' class="btn btn-danger btn-sm mr-1 delete" href="javascript:void(0)" title="Delete"><i class="fas fa-trash-alt white cicon"></i></a>
                    </div>';
                    return $btn;
                })
                ->rawColumns(['created_by','action'])
                ->make(true);
        }
        return view('products.index')->with(['title'=>'Products']);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $post=$request->all();
            $post['created_by']= Auth::user()->id;
            $request->id ?? 0;
            $product=Product::updateOrCreate(['id'=>$request->id],$post);
            $message=$request->id=='' ? 'Product successfully Inserted' : 'Product successfully Updated';
            return response()->json(['status' => true,'message' => $message]);
        }
        catch(Throwable $th){
            return response()->json(['status'=> false,'message' => $th->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try{
            $data=Product::whereId($id)->firstorFail();
            return response()->json(['status'=>true,'message'=>"Successfully Fetched Data",'data'=>$data]);
        }
        catch(Throwable $th){
            return response()->json(['status'=>false,'message' => $th->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Product::find($id)->delete();
        return response()->json(['status' => true, 'message' => 'Successfully Deleted Product']);
    }
}
